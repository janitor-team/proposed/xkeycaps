/* xkeycaps, Copyright (c) 1991, 1992, 1993, 1997, 1998
 *  Jamie Zawinski <jwz@jwz.org>
 *
 * This file describes the keycodes of a Sony PCG-C1V ("Picturebook")
 * series keyboard.
 * by Christoph Berg <cb@cs.uni-sb.de> 010901
 * (copied from pc105-codes.h)
 */

static const KeyCode c1v_codes [] = {
  /* Row 0 */
  9,
  67, 68, 69, 70,
  71, 72, 73, 74,
  75, 76, 95, 96,
  77, 111, 106, 107,
  78, 110, 114, /* Fn */

  /* Row 1 */
  10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,

  /* Row 2 */
  23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,

  /* Row 3 */
  66, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 51,

  /* Row 4 */
  50, 94, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61,
  0, 98, 62,
  99, /* Fn: PgUp */

  /* Row 5 */
  37, 0, 49, 115,
  64, 65, 113, 117,
  109,
  100, 104, 102,
  97, 105, 103 /* Fn: Home, PgDn, End */
};
